﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConfirmScript : MonoBehaviour {

	// Use this for initialization
	void Start () {     
        Cursor.visible = true;        
    }
	
    public void Yes()
    {
        //Application.Quit();
        #if UNITY_EDITOR
                // Application.Quit() does not work in the editor so
                // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
                UnityEditor.EditorApplication.isPlaying = false;
        #else
                         Application.Quit();
        #endif
    }

    public void No()
    {
        SceneManager.LoadScene("MenuGame");
    }
}
