﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Castle : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            int t = GlobalInfo.store_round;
            t += 1;
            if (t==4)
            {
                SceneManager.LoadScene("Win");
                GlobalInfo.UpdateRound(0);
            }
            else
            {
                SceneManager.LoadScene("Level" + t.ToString());
                GlobalInfo.UpdateRound(t);
            }        
            
        }
    }
}
