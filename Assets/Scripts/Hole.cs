﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Hole : MonoBehaviour {

    AudioClip music;
    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            AudioSource audio = GetComponent<AudioSource>();
            music = Resources.Load<AudioClip>("Audio/smb_mariodie");
            audio.PlayOneShot(music);
            SceneManager.LoadScene("GameOver");
        }
    }
}
