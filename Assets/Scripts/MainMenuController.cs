﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

    void Start()
    {
        Cursor.visible = true;
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MenuGame");
    }

    public void Retry()
    {
        int r = GlobalInfo.store_round;
        SceneManager.LoadScene("Level"+r.ToString());
        GlobalInfo.UpdateRound(r);
        GlobalInfo.SetScore(0);
    }

    public void NewGame() {
        SceneManager.LoadScene("Level0");
        GlobalInfo.UpdateRound(0);
        GlobalInfo.SetScore(0);
    }

    public void SaveGame()
    {
        SaveLoad.Save();
    }

    public void LoadGame()
    {
        SaveLoad.Load();
    }

    public void Exit()
    {
        SceneManager.LoadScene("ConfirmQuit");
    }
}
