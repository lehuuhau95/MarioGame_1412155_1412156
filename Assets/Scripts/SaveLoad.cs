﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;

public static class SaveLoad {
    public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        Game save_game = new Game();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd");
        bf.Serialize(file, save_game);
        file.Close();
    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            Game loaded_game = new Game();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            loaded_game = (Game)bf.Deserialize(file);
            GlobalInfo.store_score = loaded_game.save_score;
            GlobalInfo.store_round = loaded_game.save_round;
            LoadScreen(GlobalInfo.store_round);
            file.Close();
        }
    }

    static void LoadScreen(int round)
    {
        SceneManager.LoadScene("Level"+round.ToString());
    }
}
