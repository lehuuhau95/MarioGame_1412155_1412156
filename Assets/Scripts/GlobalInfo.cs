﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalInfo  {
    public static int store_score = 0;
    public static int store_round = 0;

    public static void UpdateScore()
    {
        store_score += 1;
    }

    public static void SetScore(int score)
    {
        store_score = score;
    }

    public static void UpdateRound(int round)
    {
        store_round = round;
    }
}
