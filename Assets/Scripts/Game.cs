﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Game {
    public int save_score;
    public int save_round;
    
    public Game()
    {
        save_score = GlobalInfo.store_score;
        save_round = GlobalInfo.store_round;
    }
}
